from django.db import models

class Perhitungan(models.Model):
    a = models.IntegerField(default=0)
    b = models.IntegerField(default=0)
    tipe = models.CharField(max_length=1, default='-')
    ans = models.IntegerField(default=0)

    def __str__(self):
        return str(self.id)
